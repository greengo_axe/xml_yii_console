# XML, bash and YII2 Console App

## Abstract 
To import xml data into a MySQL/MariaDb database through YII2 Console, bash and crontab

## Premise
This project requires :

* PHP 7.x
* MySql/MariaDb 5.x
* Composer

They have to be up and running. 

## Installation
*  Download and install Yii2 framework(advanced template), via composer, on your local computer . For all info click [here](https://yiiframework.com) 
 *  The root folder of the project is 
 
       >**xml_yii_console** 
    
 * and the absolute path is
 
      >**/var/www/html/yiiconsole/xml_yii_console**
    
  * Clone the project from gitlab on Desktop :
    
    >**git@gitlab.com:greengo_axe/xml_yii_console.git**

* copy and overwrite all folders and files from 

>>**Desktop/xml_yii_console**

>into 

>>**/var/www/html/yiiconsole/xml_yii_console**

* Delete the folder on Desktop (**Desktop/xml_yii_console**)

* Change the permissions:
> **cd /var/www/html/yiiconsole/xml_yii_console**
> 
> **chmod 777 yii**

* Import the database structure (**xml_yii_console/xmlconsole.sql**) into MySQL/MariaDb via PHPMyAdmin or via console

> /var/www/html/yiiconsole/xml_yii_console/common/config/main-local.php # db settings

* Set the crontab:

> **crontab -e**

> **\*/1 * * * \* /usr/bin/bash /var/www/html/yiiconsole/xml_yii_console/console/scripts/process_xml.sh** #every minute

* Enjoy!

## Troubleshooting
This project has been developed on Linux OS and there might be a few problems with the path or to grant the right permissions on Windows / Apple OSs.

* You're free to change the path of the project. If so, you have to replace the new path in the following files:

>* console/controllers/**xmlController.php**

>* console/scripts/**process_xml.sh**

* The direct command of Yii2 Console is:

>> **/path/to/folder/xml_yii_console/yii xml/savetodb**

>Ex.:

>>**/var/www/html/yiiconsole/xml_yii_console/yii xml/savetodb**

